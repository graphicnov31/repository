package com.jpmorgan.supersimplestocks.business.model;

import java.math.BigDecimal;

/**
 * The Class Stock.
 */
public class Stock {

	/** The simbol. */
	private String simbol;

	/** The type. */
	private StockType type;

	/** The last dividend. */
	private BigDecimal lastDividend;

	/** The fixed dividend. */
	private BigDecimal fixedDividend;

	/** The par value. */
	private BigDecimal parValue;

	/** The ticker price. */
	private BigDecimal tickerPrice;

	/**
	 * Instantiates a new stock.
	 */
	public Stock() {

	}

	/**
	 * Instantiates a new stock.
	 * 
	 * @param simbol
	 *            the simbol
	 * @param type
	 *            the type
	 * @param lastDividend
	 *            the last dividend
	 * @param fixedDividend
	 *            the fixed dividend
	 * @param parValue
	 *            the par value
	 * @param tickerPrice
	 *            the ticker price
	 */
	public Stock(String simbol, StockType type, BigDecimal lastDividend,
			BigDecimal fixedDividend, BigDecimal parValue,
			BigDecimal tickerPrice) {
		this.simbol = simbol;
		this.type = type;
		this.lastDividend = lastDividend;
		this.fixedDividend = fixedDividend;
		this.parValue = parValue;
		this.tickerPrice = tickerPrice;
	}

	/**
	 * Gets the simbol.
	 * 
	 * @return the simbol
	 */
	public String getSimbol() {
		return simbol;
	}

	/**
	 * Sets the simbol.
	 * 
	 * @param simbol
	 *            the new simbol
	 */
	public void setSimbol(String simbol) {
		this.simbol = simbol;
	}

	/**
	 * Gets the type.
	 * 
	 * @return the type
	 */
	public StockType getType() {
		return type;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            the new type
	 */
	public void setType(StockType type) {
		this.type = type;
	}

	/**
	 * Gets the last dividend.
	 * 
	 * @return the last dividend
	 */
	public BigDecimal getLastDividend() {
		return lastDividend;
	}

	/**
	 * Sets the last dividend.
	 * 
	 * @param lastDividend
	 *            the new last dividend
	 */
	public void setLastDividend(BigDecimal lastDividend) {
		this.lastDividend = lastDividend;
	}

	/**
	 * Gets the fixed dividend.
	 * 
	 * @return the fixed dividend
	 */
	public BigDecimal getFixedDividend() {
		return fixedDividend;
	}

	/**
	 * Sets the fixed dividend.
	 * 
	 * @param fixedDividend
	 *            the new fixed dividend
	 */
	public void setFixedDividend(BigDecimal fixedDividend) {
		this.fixedDividend = fixedDividend;
	}

	/**
	 * Gets the par value.
	 * 
	 * @return the par value
	 */
	public BigDecimal getParValue() {
		return parValue;
	}

	/**
	 * Sets the par value.
	 * 
	 * @param parValue
	 *            the new par value
	 */
	public void setParValue(BigDecimal parValue) {
		this.parValue = parValue;
	}

	/**
	 * Gets the ticker price.
	 * 
	 * @return the ticker price
	 */
	public BigDecimal getTickerPrice() {
		return tickerPrice;
	}

	/**
	 * Sets the ticker price.
	 * 
	 * @param tickerPrice
	 *            the new ticker price
	 */
	public void setTickerPrice(BigDecimal tickerPrice) {
		this.tickerPrice = tickerPrice;
	}

}
