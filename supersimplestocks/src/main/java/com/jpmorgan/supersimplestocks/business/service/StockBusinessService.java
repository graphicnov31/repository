package com.jpmorgan.supersimplestocks.business.service;

import java.math.BigDecimal;
import java.util.Date;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Interface StockBusinessService.
 */
public interface StockBusinessService {

	/**
	 * Calculates the dividend yield.
	 * 
	 * @param stock
	 *            the stock
	 * @return the big decimal
	 * @throws ExecuteException
	 */
	BigDecimal calculateDividendYield(Stock stock) throws ExecuteException;

	/**
	 * Calculates P/E ratio.
	 * 
	 * @param stock
	 *            the stock
	 * @return the big decimal
	 * @throws ExecuteException
	 */
	BigDecimal calculatePERatio(Stock stock) throws ExecuteException;

	/**
	 * Records trade.
	 * 
	 * @param stock
	 *            the stock
	 * @param timestamp
	 *            the timestamp
	 * @param sharesQuantity
	 *            the shares quantity
	 * @param indicatorType
	 *            the indicator type
	 * @param price
	 *            the price
	 * @throws ExecuteException
	 */
	void recordTrade(Stock stock, Date timestamp, int sharesQuantity,
			TradeIndicatorType indicatorType, BigDecimal price)
			throws ExecuteException;

	/**
	 * Calculates stock price in a given range.
	 * 
	 * @param stock
	 *            the stock
	 * @param startTime
	 *            the start time
	 * @param endTime
	 *            the end time
	 * @return the big decimal
	 * @throws ExecuteException
	 */
	BigDecimal calculateStockPrice(Stock stock, Date startTime, Date endTime)
			throws ExecuteException;

	/**
	 * Calculates GBCE in a given range.
	 * 
	 * @param startTime
	 *            the start time
	 * @param endTime
	 *            the end time
	 * @return the big decimal
	 * @throws ExecuteException
	 */
	BigDecimal calculateGBCE(Date startTime, Date endTime)
			throws ExecuteException;
}
