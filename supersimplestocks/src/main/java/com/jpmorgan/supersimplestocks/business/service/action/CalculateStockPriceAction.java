package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;
import java.util.Date;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Interface CalculateStockPriceAction.
 */
public interface CalculateStockPriceAction extends Action {

	/**
	 * Calculates stock price in a given range.
	 * 
	 * @param stock
	 *            the stock
	 * @param startTime
	 *            the start time
	 * @param endTime
	 *            the end time
	 * @return the big decimal
	 * @throws ExecuteException
	 */
	BigDecimal calculateStockPrice(Stock stock, Date startTime, Date endTime)
			throws ExecuteException;
}
