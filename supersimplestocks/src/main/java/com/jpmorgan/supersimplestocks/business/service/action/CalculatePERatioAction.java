package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Interface CalculatePERatioAction.
 */
public interface CalculatePERatioAction extends Action {

	/**
	 * Calculates P/E ratio.
	 * 
	 * @param stock
	 *            the stock
	 * @return the big decimal
	 * @throws ExecuteException
	 */
	BigDecimal calculatePERatio(Stock stock) throws ExecuteException;
}
