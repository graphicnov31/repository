package com.jpmorgan.supersimplestocks.business.model;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The Class Trade.
 */
public class Trade {

	/** The timestamp. */
	private Date timestamp;

	/** The indicator type. */
	private TradeIndicatorType indicatorType;

	/** The shares quantity. */
	private int sharesQuantity;

	/** The price. */
	private BigDecimal price;

	/**
	 * Instantiates a new trade.
	 */
	public Trade() {

	}

	/**
	 * Instantiates a new trade.
	 * 
	 * @param timestamp
	 *            the timestamp
	 * @param indicatorType
	 *            the indicator type
	 * @param sharesQuantity
	 *            the shares quantity
	 * @param price
	 *            the price
	 * @param stock
	 *            the stock
	 */
	public Trade(Date timestamp, TradeIndicatorType indicatorType,
			int sharesQuantity, BigDecimal price) {
		super();
		this.timestamp = timestamp;
		this.indicatorType = indicatorType;
		this.sharesQuantity = sharesQuantity;
		this.price = price;
	}

	/**
	 * Gets the timestamp.
	 * 
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets the timestamp.
	 * 
	 * @param timestamp
	 *            the new timestamp
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Gets the indicator type.
	 * 
	 * @return the indicator type
	 */
	public TradeIndicatorType getIndicatorType() {
		return indicatorType;
	}

	/**
	 * Sets the indicator type.
	 * 
	 * @param indicatorType
	 *            the new indicator type
	 */
	public void setIndicatorType(TradeIndicatorType indicatorType) {
		this.indicatorType = indicatorType;
	}

	/**
	 * Gets the shares quantity.
	 * 
	 * @return the shares quantity
	 */
	public int getSharesQuantity() {
		return sharesQuantity;
	}

	/**
	 * Sets the shares quantity.
	 * 
	 * @param sharesQuantity
	 *            the new shares quantity
	 */
	public void setSharesQuantity(int sharesQuantity) {
		this.sharesQuantity = sharesQuantity;
	}

	/**
	 * Gets the price.
	 * 
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 * 
	 * @param price
	 *            the new price
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

}
