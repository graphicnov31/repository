package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType;
import com.jpmorgan.supersimplestocks.entity.service.StockEntityService;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Class RecordTradeActionImpl.
 */
public class RecordTradeActionImpl implements RecordTradeAction {

	/** The stock entity service. */
	@Autowired
	private StockEntityService stockEntityService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.supersimplestocks.business.service.action.RecordTradeAction
	 * #recordTrade(com.jpmorgan.supersimplestocks.business.model.Stock,
	 * java.util.Date, int,
	 * com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType,
	 * java.math.BigDecimal)
	 */
	@Override
	public void recordTrade(Stock stock, Date timestamp, int sharesQuantity,
			TradeIndicatorType indicatorType, BigDecimal price)
			throws ExecuteException {
		stockEntityService.recordTrade(stock, timestamp, sharesQuantity,
				indicatorType, price);

	}

}
