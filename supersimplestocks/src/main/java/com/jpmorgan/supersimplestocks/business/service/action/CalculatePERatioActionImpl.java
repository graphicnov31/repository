package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.service.StockBusinessService;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.NullStockException;
import com.jpmorgan.supersimplestocks.exception.OperationNotAllowedException;

/**
 * The Class CalculatePERatioActionImpl.
 */
public class CalculatePERatioActionImpl implements CalculatePERatioAction {

	/** The stock business service. */
	@Autowired
	private StockBusinessService stockBusinessService;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CalculatePERatioActionImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.supersimplestocks.business.service.action.CalculatePERatioAction
	 * #calculatePERation(com.jpmorgan.supersimplestocks.business.model.Stock)
	 */
	@Override
	public BigDecimal calculatePERatio(Stock stock) throws ExecuteException {
		BigDecimal result = null;

		// check if stock is null
		if (stock == null) {
			NullStockException nullStockException = new NullStockException();
			LOGGER.error(nullStockException.getMessage());
			throw nullStockException;
		}

		try {
			// calculate dividendYield
			BigDecimal dividend = stockBusinessService
					.calculateDividendYield(stock);

			// calculate tickerPrice / dividendYield
			result = stock.getTickerPrice().divide(dividend, 2,
					BigDecimal.ROUND_HALF_UP);
			return result;
		} catch (ArithmeticException e) {
			OperationNotAllowedException operationNotAllowedException = new OperationNotAllowedException(
					e.getMessage());
			LOGGER.error(e.getMessage());
			throw operationNotAllowedException;
		} catch (ExecuteException e) {
			OperationNotAllowedException operationNotAllowedException = new OperationNotAllowedException(
					"calculate dividendYiled failed");
			LOGGER.error(operationNotAllowedException.getMessage());
			throw operationNotAllowedException;
		}

	}

}
