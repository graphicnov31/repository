package com.jpmorgan.supersimplestocks.business.service.action;

import java.math.BigDecimal;
import java.util.Date;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Interface RecordTradeAction.
 */
public interface RecordTradeAction extends Action {

	/**
	 * Records trade.
	 * 
	 * @param stock
	 *            the stock
	 * @param timestamp
	 *            the timestamp
	 * @param sharesQuantity
	 *            the shares quantity
	 * @param indicatorType
	 *            the indicator type
	 * @param price
	 *            the price
	 * @throws ExecuteException
	 */
	void recordTrade(Stock stock, Date timestamp, int sharesQuantity,
			TradeIndicatorType indicatorType, BigDecimal price)
			throws ExecuteException;
}
