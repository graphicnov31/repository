package com.jpmorgan.supersimplestocks.exception;

/**
 * A generic Exception.
 * 
 */
public abstract class ExecuteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
