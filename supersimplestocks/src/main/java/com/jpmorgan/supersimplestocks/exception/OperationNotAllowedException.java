package com.jpmorgan.supersimplestocks.exception;

/**
 * This exception is thrown when it is not possible to do a calculating
 * operation.
 * 
 */
public class OperationNotAllowedException extends ExecuteException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The message. */
	String message = "Operation not allowed: ";

	/**
	 * Costructor.
	 * 
	 * @param message
	 *            the reason why the operation is not possible
	 */
	public OperationNotAllowedException(String reason) {
		this.message = new StringBuilder(message).append(reason).toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return this.message;
	}

}
