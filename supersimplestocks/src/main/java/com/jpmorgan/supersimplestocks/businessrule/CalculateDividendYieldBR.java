package com.jpmorgan.supersimplestocks.businessrule;

import java.math.BigDecimal;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Interface CalculateDIvidendYieldBR.
 */
public interface CalculateDividendYieldBR {

	/**
	 * Calculate dividend yield.
	 * 
	 * @param stock
	 *            the stock
	 * @return the big decimal
	 * @throws ExecuteException
	 */
	BigDecimal calculateDividendYield(Stock stock) throws ExecuteException;
}
