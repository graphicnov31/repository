package com.jpmorgan.supersimplestocks.businessrule;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.OperationNotAllowedException;

/**
 * The Class CalculateDividendYieldForPreferredBRImpl.
 */
public class CalculateDividendYieldForPreferredBRImpl implements
		CalculateDividendYieldBR {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CalculateDividendYieldForPreferredBRImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.businessrule.CalculateDIvidendYieldBR#calculateDividendYield
	 * (com.jpmorgan.supersimplestocks.business.model.Stock)
	 */
	@Override
	public BigDecimal calculateDividendYield(Stock stock)
			throws ExecuteException {
		BigDecimal result = null;

		BigDecimal fixedDividend = stock.getFixedDividend();
		BigDecimal parValue = stock.getParValue();
		BigDecimal tickerPrice = stock.getTickerPrice();

		try {
			// calculate (fixedDividend x parValue) / tickerPrice
			result = (fixedDividend.multiply(parValue)).divide(tickerPrice, 2,
					BigDecimal.ROUND_HALF_UP);
		} catch (ArithmeticException e) {
			OperationNotAllowedException operationNotAllowedException = new OperationNotAllowedException(
					e.getMessage());
			LOGGER.error(operationNotAllowedException.getMessage());
			throw operationNotAllowedException;
		}
		return result;
	}

}
