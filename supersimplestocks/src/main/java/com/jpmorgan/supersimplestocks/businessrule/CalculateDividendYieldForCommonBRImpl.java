package com.jpmorgan.supersimplestocks.businessrule;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.OperationNotAllowedException;

/**
 * The Class CalculateDividendYieldForCommonBRImpl.
 */
public class CalculateDividendYieldForCommonBRImpl implements
		CalculateDividendYieldBR {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CalculateDividendYieldForCommonBRImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmorgan.businessrule.CalculateDIvidendYieldBR#calculateDividendYield
	 * (com.jpmorgan.supersimplestocks.business.model.Stock)
	 */
	@Override
	public BigDecimal calculateDividendYield(Stock stock)
			throws ExecuteException {
		BigDecimal result = null;

		BigDecimal lastDividend = stock.getLastDividend();
		BigDecimal tickerPrice = stock.getTickerPrice();

		try {
			// calculate lastDividend / tickerPrice
			result = lastDividend.divide(tickerPrice, 2,
					BigDecimal.ROUND_HALF_UP);
		} catch (ArithmeticException e) {
			OperationNotAllowedException operationNotAllowedException = new OperationNotAllowedException(
					e.getMessage());
			LOGGER.error(operationNotAllowedException.getMessage());
			throw operationNotAllowedException;
		}
		return result;
	}

}