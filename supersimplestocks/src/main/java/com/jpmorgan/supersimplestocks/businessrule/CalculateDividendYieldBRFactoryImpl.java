package com.jpmorgan.supersimplestocks.businessrule;

import org.springframework.beans.factory.annotation.Autowired;

import com.jpmorgan.supersimplestocks.business.model.StockType;

/**
 * The Class CalculateDividendYieldBRFactoryImpl.
 */
public class CalculateDividendYieldBRFactoryImpl implements
		CalculateDividendYieldBRFactory {

	/** The calculate dividend yield for common br. */
	@Autowired
	private CalculateDividendYieldBR calculateDividendYieldForCommonBR;

	/** The calculate dividend yield for preferred br. */
	@Autowired
	private CalculateDividendYieldBR calculateDividendYieldForPreferredBR;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmorgan.businessrule.CalculateDividendYieldBRFactory#
	 * calculateDIvidendYieldBRFactory
	 * (com.jpmorgan.supersimplestocks.business.model.StockType)
	 */
	@Override
	public CalculateDividendYieldBR calculateDividendYieldBRFactory(
			StockType stockType) {

		switch (stockType) {
		case COMMON:
			return calculateDividendYieldForCommonBR;
		case PREFERRED:
			return calculateDividendYieldForPreferredBR;
		default:
			return null;
		}
	}
}
