package com.jpmorgan.supersimplestocks.entity.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.Trade;
import com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;

/**
 * The Interface StockEntityService.
 */
public interface StockEntityService {

	/**
	 * Records trade.
	 * 
	 * @param stock
	 *            the stock
	 * @param timestamp
	 *            the timestamp
	 * @param sharesQuantity
	 *            the shares quantity
	 * @param indicatorType
	 *            the indicator type
	 * @param price
	 *            the price
	 * @throws ExecuteException
	 */
	void recordTrade(Stock stock, Date timestamp, int sharesQuantity,
			TradeIndicatorType indicatorType, BigDecimal price)
			throws ExecuteException;

	/**
	 * Gets the stocks.
	 * 
	 * @return the stocks
	 */
	Collection<Stock> getStocks();

	/**
	 * Gets the trades by stock.
	 * 
	 * @param stock
	 *            the stock
	 * @return the trades by stock
	 */
	Collection<Trade> getTradesByStock(Stock stock);

	/**
	 * Gets the trades in range.
	 * 
	 * @param stock
	 *            the stock
	 * @param startTime
	 *            the start time
	 * @param endTime
	 *            the end time
	 * @return the trades in range
	 */
	Collection<Trade> getTradesInRange(Stock stock, Date startTime, Date endTime);

}
