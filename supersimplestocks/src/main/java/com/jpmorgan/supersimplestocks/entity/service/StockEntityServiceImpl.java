package com.jpmorgan.supersimplestocks.entity.service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cglib.core.CollectionUtils;
import org.springframework.cglib.core.Predicate;

import com.jpmorgan.supersimplestocks.business.model.Stock;
import com.jpmorgan.supersimplestocks.business.model.Trade;
import com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType;
import com.jpmorgan.supersimplestocks.business.service.action.CalculateDividendYieldActionImpl;
import com.jpmorgan.supersimplestocks.exception.ExecuteException;
import com.jpmorgan.supersimplestocks.exception.NullStockException;

/**
 * The Class StockEntityServiceImpl.
 */
public class StockEntityServiceImpl implements StockEntityService {

	/** The stocks map. */
	private Map<Stock, Set<Trade>> stocksMap;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CalculateDividendYieldActionImpl.class);

	/**
	 * Instantiates a new stock entity service impl.
	 */
	public StockEntityServiceImpl() {
		stocksMap = new HashMap<Stock, Set<Trade>>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmotgan.supersimplestocks.entity.service.StockEntityService#recordTrade
	 * (com.jpmorgan.supersimplestocks.business.model.Stock, java.util.Date,
	 * int, com.jpmorgan.supersimplestocks.business.model.TradeIndicatorType,
	 * java.math.BigDecimal)
	 */
	@Override
	public void recordTrade(Stock stock, Date timestamp, int sharesQuantity,
			TradeIndicatorType indicatorType, BigDecimal price)
			throws ExecuteException {

		// check if stock is null
		if (stock == null) {
			NullStockException nullStockException = new NullStockException();
			LOGGER.error(nullStockException.getMessage());
			throw nullStockException;
		}

		// create trade
		Trade trade = new Trade(timestamp, indicatorType, sharesQuantity, price);
		// retrieve tradesSet of the stock
		Set<Trade> tradesSet = stocksMap.get(stock);
		// if there are not trades
		if (tradesSet == null) {
			// create tradesSet
			tradesSet = new HashSet<Trade>();
			// add tradeSet to the stock
			stocksMap.put(stock, tradesSet);
		}
		// add trade to the tradesSet
		tradesSet.add(trade);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.jpmotgan.supersimplestocks.entity.service.StockEntityService#getStocks
	 * ()
	 */
	@Override
	public Collection<Stock> getStocks() {
		return stocksMap.keySet();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmotgan.supersimplestocks.entity.service.StockEntityService#
	 * getTradesByStock(com.jpmorgan.supersimplestocks.business.model.Stock)
	 */
	@Override
	public Collection<Trade> getTradesByStock(Stock stock) {
		return stocksMap.get(stock);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.jpmotgan.supersimplestocks.entity.service.StockEntityService#
	 * getTradesInRange(com.jpmorgan.supersimplestocks.business.model.Stock,
	 * java.util.Date, java.util.Date)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Trade> getTradesInRange(final Stock stock,
			final Date startTime, final Date endTime) {
		// filter map according to predicate
		return CollectionUtils.filter(stocksMap.get(stock), new Predicate() {

			@Override
			public boolean evaluate(final Object object) {
				final Trade trade = (Trade) object;

				return (startTime != null && startTime.compareTo(trade
						.getTimestamp()) <= 0)
						&& (endTime != null && trade.getTimestamp().compareTo(
								endTime) <= 0);
			}
		});
	}

}
